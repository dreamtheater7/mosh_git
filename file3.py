# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("this is just a application program.")

    # with recursion:
    def fibonacci_recursive(idx):
        if idx < 2:
            return idx
        else:
            return fibonacci_recursive(idx-2)+fibonacci_recursive(idx-1)

    inp = 8
    result = fibonacci_recursive(inp)
    print(f"the fibonacci number of {inp} is {result}")

    #the same with iteration:
    def fibonacci_iteration(n):
        seq = [0,1]
        for i in range(n):
            seq.append(seq[-2]+seq[-1])
        return seq[-2]

    inp_i = 8
    result_i = fibonacci_iteration(inp_i)
    print(f"the fibonacci number of {inp_i} is {result_i}")

if __name__ == "__main__":
    main()

